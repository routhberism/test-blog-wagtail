from django.db import models
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.template import Library

from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager
from taggit.models import Tag as TaggitTag
from taggit.models import TaggedItemBase
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel,

)
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.models import Page

register = Library()


class BlogPage(RoutablePageMixin, Page):

    def pagitation(self,request, posts):
        
        paginator = Paginator(posts, 2)
        page = request.GET.get("page")
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.object_list.none()
        
        return posts

    def get_context(self, request, *args, **kwargs):
        categories = BlogCategory.objects.all()
        posts = PostPage.objects.all()
        context = super().get_context(request, *args, **kwargs)
        context['categories'] = categories
        context['blog_page'] = self

        post = self.pagitation(request, posts)
        
        if request.get_full_path() == "/blog/":
            context["posts"] = post
        elif '/blog/category/' in request.get_full_path():
            context["posts"] = self.posts
        
        from blog.templatetags.blogapp_tags import navbar_categories
        context['context'] = navbar_categories(context)
        return context

    def get_posts(self):
        return PostPage.objects.all()

    @route(r'^category/(?P<category>[-\w]+)/$')
    def post_by_category(self, request, category, *args, **kwargs):
        self.posts = self.get_posts().filter(categories__blog_category__name=category)
        self.pagitation(request, self.posts)
        return self.render(request)

class PostPage(Page):

    def get_context(self, request, *args, **kwargs):
        categories = BlogCategory.objects.all()
        context = super().get_context(request, *args, **kwargs)
        context['categories']= categories
        return context

    header_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through="blog.PostPageTag", blank=True)

    content_panels = Page.content_panels + [
        ImageChooserPanel("header_image"),
        FieldPanel("body", classname="full"),
        InlinePanel("categories", label="category"),
        FieldPanel("tags"),
    ]

class PostPageBlogCategory(models.Model):
    page = ParentalKey(
        "blog.PostPage", on_delete=models.CASCADE, related_name="categories"
    )
    blog_category = models.ForeignKey(
        "blog.BlogCategory", on_delete=models.CASCADE, related_name="post_pages"
    )

    panels = [
        SnippetChooserPanel("blog_category"),
    ]

    class Meta:
        unique_together = ("page", "blog_category")

@register_snippet
class BlogCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=80)

    panels = [
        FieldPanel("name"),
        FieldPanel("slug"),
    ]

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

class PostPageTag(TaggedItemBase):
    content_object = ParentalKey("PostPage", related_name="post_tags")

@register_snippet
class Tag(TaggitTag):
    class Meta:
        proxy = True
