# -*- coding: utf-8 -*-
from django.template import Library, loader

# from el_pagination.templatetags.el_pagination_tags import show_pages, paginate

from ..models import BlogCategory as Category

register = Library()

@register.inclusion_tag('blog/components/navbar_categories.html', takes_context=True)
def navbar_categories(context):
    blog_page = context['blog_page']
    categories = Category.objects.all()
    print(context['blog_page'])
    return {'blog_page':blog_page,'context': context, 'categories': categories}
