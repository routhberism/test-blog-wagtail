from django.db import models
from wagtail.core.models import Page


class HomePage(Page):
   def get_template(self, request, *args, **kwargs):
         return 'home/home_page.html'
    